#!/bin/sh

if [ "$1" = "" ]; then
  echo "You must pass season and episode code, like this: 8x9"
  exit 1
fi

URL="https://watchimpracticaljokers.com/episodes/impractical-jokers-$1/"

PAGE_SOURCE="$(curl -s "$URL")"
EPISODE_CODE="$(echo $PAGE_SOURCE | pup "#info h1.epih1 text{}")"
EPISODE_TITLE="$(echo $PAGE_SOURCE | pup "#info h3.epih3 text{}")"
EPISODE_DESC="$(echo $PAGE_SOURCE | pup "#info p text{}")"
POST_ID="$(echo $PAGE_SOURCE | pup "#player-option-1 attr{data-post}")"

EMBED_URL="$(curl -s 'https://watchimpracticaljokers.com/wp-admin/admin-ajax.php' \
  -H 'content-type: application/x-www-form-urlencoded; charset=UTF-8' \
  --data-raw "action=doo_player_ajax&post=${POST_ID}&nume=1&type=tv" | jq -r ".embed_url")"

STREAM_URI="$(curl -s $EMBED_URL | pcregrep -o1 '"hls":.*"(.*)"')"

echo $EPISODE_CODE
echo $EPISODE_TITLE
echo $EPISODE_DESC
echo

mpv --title="$EPISODE_CODE $EPISODE_TITLE" "$STREAM_URI"
