#!/bin/sh

SINK=$(pacmd list-sinks | grep "index:" | grep '*' | awk '{print $3}')
CURRENT=$(pacmd dump-volumes | awk 'NR==1{print $8}' | sed 's/\%//')


if [ "$SINK" == "" ]; then
  SINK="0"
fi

if [ "$1" != "M" ]; then
  pactl set-sink-mute "$SINK" 0
  if [ $1 == "dec" ]; then
    pactl set-sink-volume $SINK -5%
  elif [ $1 == "inc" ]; then
    [ $CURRENT -lt 120 ] && pactl set-sink-volume $SINK +5%
  fi  
  # [ $CURRENT -le 120 ] && pactl set-sink-volume "$SINK" "$1"
else
  pactl set-sink-mute "$SINK" toggle
fi


#[ $current -lt 120 ] && pactl set-sink-volume $SINK +5%
