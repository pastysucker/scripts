#!/bin/bash

if [ "$1" == "" ]; then
  echo "tube.perverzija.com url argument required!"
  exit 1
fi

URL="$1"

# Get embedded video page
#  NOTE: Tested with XtremeStream embeds for now
EMBEDDED_URL=$(curl -s $URL | pup "#player-embed > iframe attr{src}")
EMBEDDED_SOURCE=$(curl -s $EMBEDDED_URL -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:89.0) Gecko/20100101 Firefox/89.0' -H 'Referer: https://tube.perverzija.com/')

VIDEO_ID=$(echo $EMBEDDED_SOURCE | pcregrep -o1 'video_id = `(.*?)`')
M3U8_LOADER_URL=$(echo $EMBEDDED_SOURCE | pcregrep -o1 'm3u8_loader_url = `(.*?)`')

# TODO maybe check resolution instead of hardcoding
M3U_URL="${M3U8_LOADER_URL}${VIDEO_ID}&q=1080"
M3U_FILE="$(curl -s "$M3U_URL" -H 'Referer: https://perverzija.xtremestream.co/')"

STREAM_LINKS="$(echo "$M3U_FILE" | grep '^https')"

# Start downloading
echo "$STREAM_LINKS" | aria2c -i - -x4 -Z --auto-file-renaming=false --continue

# Get a list of files ready for ffmpeg
fd ".*.html" | awk '{print "file ./" $0}' - > links.txt

# Concatenate all the files and spit out the final video
ffmpeg -f concat -safe 0 -i links.txt -c copy output.mp4

